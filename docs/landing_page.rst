Overview
========

The `dkist-processing-math` code repository contains a set of common mathematical functions
designed to operate efficiently over batches of array data.  The math library is used
throughout the DKIST data calibration software stack.
